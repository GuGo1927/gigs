export default {
  getPlaces: state => {
    return state.places;
  },

  getList: state => {
    return state.list;
  },

  getConcertsLength: state => {
    if(state.filteredConcertsLength != null && state.filteredConcertsLength >= 0)
      return state.filteredConcertsLength;
    else
      return state.concerts.length;
  },

  getLocations: state => {
    return state.locations;
  },

  getPerPage: state => {
    return state.perPage;
  },

  getPageLimit: state => {
    return state.limit;
  }
};
