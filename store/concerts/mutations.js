export default {
  setConcerts(state, payload) {
    state.list = state.list.concat(state.concerts.slice((payload.perPage - 1) * payload.limit, payload.perPage * payload.limit));
  },

  setLocations(state, payload) {
    if(payload.length > 0)
      state.locations = state.places.filter(val => val.city.toLowerCase().includes(payload.toLowerCase()));
    else
      state.locations = [];
  },

  setFilters(state, payload) {
    state.filters.forEach((val, key) => {
      if (val.filter === payload.filter) {
        state.filters[key].value = payload.value;
      }
    });
  },

  setFilteredConcerts(state) {
    state.list = [];

    let equalDates = (dates, concert) => {
      let currentDate = new Date(concert.startDate);
      let minDate = new Date(dates[0]);
      let maxDate = new Date(dates[1]);

      if(currentDate > minDate && currentDate < maxDate)
        return true;
      else
        return false;
    }

    let data = state.concerts.filter(val => {
      if(state.filters[0].value !== '' && state.filters[1].value !== '') {
        if(val.city === state.filters[1].value && equalDates(state.filters[0].value, val)) {
          return val;
        }
      }
      else if(state.filters[0].value !== '') {
        if(equalDates(state.filters[0].value, val)) {
          return val;
        }
      }
      else if(state.filters[1].value !== '') {
        if(val.city === state.filters[1].value) {
          return val;
        }
      }
    });

    if(data.length >= 0)
      state.filteredConcertsLength = data.length;

    if(!state.filters[1].value && !state.filters[0].value) {
      state.filteredConcertsLength = null;
      state.list = state.list.concat(state.concerts.slice(0, 10));
    }

    state.list = state.list.concat(data);
  },

  removeFilters(state, payload) {
    state.filters.forEach((val, key) => {
      if(val.filter === payload) {
        state.filters[key].value = '';
      }
    })
    state.perPage = 1;
  },

  setPerPage(state) {
    state.perPage += 1;
  }
};
