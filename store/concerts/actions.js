export default {
  loadConcerts({commit, dispatch}, payload) {
    commit('setConcerts', payload)
  },

  loadLocations({commit, dispatch}, payload) {
    commit('setLocations', payload)
  },

  async filterConcerts({commit, dispatch}, payload) {
    await commit('setFilters', payload)
    await commit('setFilteredConcerts')
  },

  async clearFilters({commit, dispatch}, payload) {
    await commit('removeFilters', payload)
    await commit('setFilteredConcerts')
  },
};
