# gigs

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

## Способ интеграции в проект

В папке components лежит все что связано с данной страницей. Но также есть глобальные компоненты (Datepicker, Dropdown, Empty, LoadingBar, Location)
Данные компоненты можно потом использовать в других модулях. Код который написан в pages/index.vue просто нужно скопировать и поставить в соответствуюший роут вашего приложения. 
Также все обработки все функции, а также все запросы которые потом будете делать, все это лежит в конкретном модуле vuex для данной страницы store/concerts
Картинки лежат в папке /static
